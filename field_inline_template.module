<?php

use \Drupal\Core\Field\FormatterInterface;
use \Drupal\Core\Field\FieldDefinitionInterface;
use \Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_field_formatter_settings_summary_alter().
 */
function field_inline_template_field_formatter_settings_summary_alter(array &$summary, array $context) {
  $module_name = basename(__FILE__, '.module');
  $settings = $context['formatter']->getThirdPartySettings($module_name);

  if (!empty($settings['inline_template'])) {
    $summary_items = [];
    $summary_items[] = t('Inline Template: True');
    $list = [
      '#theme' => 'item_list',
      '#items' => $summary_items,
    ];

    $summary[] = $list;
  }

  return $summary;
}

/**
 * Implements hook_field_formatter_third_party_settings_form().
 */
function field_inline_template_field_formatter_third_party_settings_form(FormatterInterface $plugin, FieldDefinitionInterface $field_definition, $view_mode, array $form, FormStateInterface $form_state) {

  $templates_info = Drupal::service('field_inline_template.manager')->getTemplatesInfo();
  $template_field_files = $templates_info['template_field_files'];
  $template_codes = $templates_info['template_codes'];

  $module_name = basename(__FILE__, '.module');
  $settings = $plugin->getThirdPartySettings($module_name);
  $element['inline_template'] = [
    '#type' => 'checkbox',
    '#title' => t('Inline Template'),
    '#default_value' => $settings['inline_template'] ?? FALSE,
    '#prefix' => '<div id="edit-output">',
    '#suffix' => '</div>',
  ];

  $visible_key = 'input[name$="[third_party_settings][' . $module_name . '][inline_template]"]';
  $element['inline_template_twig'] = [
    '#type' => 'codemirror',
    '#default_value' => $settings['inline_template_twig'] ?? NULL,
    '#states' => [
      'visible' => [
        $visible_key => ['checked' => TRUE],
      ],
    ],
  ];

    $widget_html_id = $field_definition->getName();
    $element['inline_template_twig']['#attributes']['data-fit-textarea'] = $widget_html_id;


    $element['footer'] = [
      '#type' => 'container',
      '#title' => 'Twig context',
      '#attributes' => ['class' => ['twig-field-editor-footer', 'container-inline']],
      '#weight' => 10,
      '#states' => [
        'visible' => [
          $visible_key => ['checked' => TRUE],
        ],
      ],
    ];

    $element['footer']['variables'] = [
      '#type' => 'select',
      '#title' => 'Variables',
      '#options' => $template_field_files,
      '#attributes' => ['data-fit-variables' => $widget_html_id],
    ];

    $element['footer']['insert'] = [
      '#type' => 'button',
      '#value' => 'Insert',
      '#attributes' => ['data-fit-insert' => $widget_html_id],
    ];

    $element['#attached']['library'][] = 'field_inline_template/editor';
    $element['#attached']['drupalSettings']['field_inline_template']['template_codes']= $template_codes;

  return $element;
}

/**
 * Implements hook_theme().
 */
function field_inline_template_theme() {
  $module_name = basename(__FILE__, '.module');
  $module_path = \Drupal::moduleHandler()->getModule($module_name)->getPath();
  return [
    'field__inline_template' => [
      'path' => $module_path . '/templates',
      'base hook' => 'field',
    ],
  ];
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function field_inline_template_theme_suggestions_field_alter(array &$suggestions, array &$variables) {
  $module_name = basename(__FILE__, '.module');
  $element = $variables['element'];
  if (!empty($element['#third_party_settings'][$module_name]['inline_template'])) {
    $suggestions[] = 'field__inline_template';
  }
}
