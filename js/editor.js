/**
 * @file
 * Twig field behaviors.
 */

(function ($, Drupal, once, drupalSettings) {

  'use strict';

  /**
   * Behavior description.
   */
  Drupal.behaviors.twigFieldEditor = {
    attach: function (context, settings) {

      $(once('fieldInlineTemplateEditor', '[data-fit-insert]')).on( 'click', function (event) {
        let templateCodes = drupalSettings.field_inline_template.template_codes;
        var widgetId = $(this).data('fit-insert');
        var $select = $(context).find('[data-fit-variables="' + widgetId + '"]');
        var variable = $select.val();
        if (variable) {
          var $textArea = $(context).find('[data-fit-textarea="' + widgetId + '"]');
          var editor = $textArea.next()[0].CodeMirror;
          var doc = editor.getDoc();
          let selectedText = $select.val();
          let code = templateCodes[selectedText];
          if (code) {
            doc.replaceSelection(code, doc.getCursor());
          }
          // $select.val('');
        }
        event.preventDefault();
      });
    }
  };

} (jQuery, Drupal, once, drupalSettings));
