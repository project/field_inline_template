<?php

namespace Drupal\field_inline_template;

class FieldInlineTemplateManager {

  public function getTemplatesInfo() {
    $registry_service = \Drupal::service('field_inline_template.theme.registry');
    $registry_service->customSetTheme();
    $template_themes = $registry_service->get();

    $template_field_files = [];
    $template_field_files_front = [];
    $template_codes = [];
    foreach ($template_themes as $key => $value) {
      if (  $key == 'field' || str_starts_with($key, 'field_')) {
        if (isset($value['path']) && isset($value['template'])) {
          $path = $value['path'] . '/' . $value['template'] . '.html.twig';
          $code = file_get_contents($path);
          if ($key == 'field') {
            $template_field_files_front[$path] = $path;
          } else {
            $template_field_files[$path] = $path;
          }
          $template_codes[$path] = $code;
        }
      }
    }
    $template_field_files = $template_field_files_front + $template_field_files;

    return [
      'template_field_files' => $template_field_files,
      'template_codes' => $template_codes,
    ];
  }

}
