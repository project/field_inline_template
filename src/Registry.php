<?php

namespace Drupal\field_inline_template;


Class Registry extends \Drupal\Core\Theme\Registry {
  public function customSetTheme() {
    $default_theme = \Drupal::config('system.theme')->get('default');
    $this->themeName = $default_theme;
    $this->theme = $this->themeInitialization->getActiveThemeByName($default_theme);
  }
}
